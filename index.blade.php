<!DOCTYPE html>
<html>
<head>
	<title>New Portal Website</title>
<meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/css/bootstrap.min.css">
  <link rel="stylesheet" type="text/css" href="css/custome.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.0/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/js/bootstrap.min.js"></script>

  <link href="//netdna.bootstrapcdn.com/font-awesome/4.0.3/css/font-awesome.css" rel="stylesheet">

  <link href='https://fonts.googleapis.com/css?family=Denk One' rel='stylesheet'>
  <link href='https://fonts.googleapis.com/css?family=Dhurjati' rel='stylesheet'>
  <link href='https://fonts.googleapis.com/css?family=Anton' rel='stylesheet'>

</head>
<body>
<div class="container-fluid">
	<!-- Header Top Ad banner Start -->
	<div class="topbanner">
		<img src="image/top-header-gif-image.gif" class="img-responsive center-block">
	</div>
	<!--Header Top Ad banner end -->
	<!-- Logo start --> 
	<div class="jumbtron toplogo">
		<h1 align="center">खबर आजको </h1>
		<p align="center"> सबै भन्दा पहिले </p>
	</div>
	<!--Logo End -->

	<!-- Nav Start-->
	<nav class="navbar nav navbar-inverse">
  <div class="container-fluid">
    <div class="navbar-header">
      <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>                        
      </button>
    </div>
    <div class="collapse navbar-collapse" id="myNavbar">
      <ul class="nav navbar-nav">
        <li class="active"><a href="#">Home</a></li>
        <li class="dropdown">
          <a class="dropdown-toggle" data-toggle="dropdown" href="#">समाचार<span class="caret"></span></a>
          <ul class="dropdown-menu">
            <li><a href="#">समाज</a></li>
            <li><a href="#">खेलकुद़़</a></li>
            <li><a href="#">जीवनशैली/स्वास्थ्य</a></li>
            <li><a href="#">प्रवास</a></li>
            <li><a href="#">अन्तराष्ट्रिय</a></li>
          </ul>
        </li>
                <li class="dropdown">
          <a class="dropdown-toggle" data-toggle="dropdown" href="#">समाचार<span class="caret"></span></a>
          <ul class="dropdown-menu">
            <li><a href="#">समाज</a></li>
            <li><a href="#">खेलकुद़़</a></li>
            <li><a href="#">जीवनशैली/स्वास्थ्य</a></li>
            <li><a href="#">प्रवास</a></li>
            <li><a href="#">अन्तराष्ट्रिय</a></li>
          </ul>
        </li>
        <li><a href="#">विजनेश</a></li>
        <li><a href="#">जीवनशैली</a></li>
        <li><a href="#">सूचना प्रविधि</a></li>
        <li><a href="#">मनोरन्जन</a></li>
        <li><a href="">प्रवास</a></li>
        <li><a href="#">खेलकुद</a></li>
      </ul>
      <ul class="nav navbar-nav navbar-right">
        <li><a href="#"><span class="glyphicon glyphicon-user"></span> Sign Up</a></li>
        <li><a href="#"><span class="glyphicon glyphicon-log-in"></span> Login</a></li>
      </ul>
    </div>
  </div>
</nav>
	<!-- Nav End -->
	<!-- Second Ad Start -->
	<div class="row secondad">
		<img src="image/ad 1.gif" class="img-responsive center-block">
	</div>
	<!--Second Ad End -->

	<!--HeadLine Start  -->

	<div class="row headline">
		<div class="col-sm-6">
			<button class="btn btn-danger btn-lg">हेडलाइन</button>

		</div>
		<div class="col-sm-6" align="right">
			<button class="btn btn-primary btn-md moreButton">सबै </button>
		</div>
	</div>
	<hr>
	<div class="row headlineNews">
		<div class="col-sm-3">
			<div class="row">
				<img src="image/headline1.jpg" width="100%">
			</div>
			<div class="row">
				<a href=""><h3 align="center">संस्कृति मास्न र जग्गा हडप्न भूमाफियाको डिजाइनमा गुठी विधेयक ! </h3></a>
			</div>
		</div>

		<div class="col-sm-3">
			<div class="row">
				<img src="image/headline2.JPG" width="100%">
			</div>
			<div class="row">
				<a href=""><h3 align="center">लोकसेवा विज्ञापनको विरोधीले मागे मन्त्री र आयोगका अध्यक्षको राजीनामा, यस्तो छ भनाई (फोटो/भिडियो) </h3></a>
			</div>
		</div>

		<div class="col-sm-3">
			<div class="row">
				<img src="image/headline3.jpg" width="100%">
			</div>
			<div class="row">
				<a href=""><h3 align="center">‘संसदीय व्यवस्थाले गणतन्त्रको लक्ष्य पूरा हुन सक्दैन’  </h3></a>
			</div>
		</div>

		<div class="col-sm-3">
			<div class="row">
				<img src="image/headline4.JPG" width="100%">
			</div>
			<div class="row">
				<a href=""><h3 align="center">प्रधानमन्त्री रोजगार कार्यक्रम : कतिले रोजगारी पाए भन्ने तथ्याङ्कसमेत सरकारसँग छैन  </h3></a>
			</div>
		</div>
				<div class="col-sm-3">
			<div class="row">
				<img src="image/headline8.jpg" width="100%">
			</div>
			<div class="row">
				<a href=""><h3 align="center">रबि लामिछानेसँग जोडेर प्रकाशित समाचारप्रति मन्त्री पुनको आपत्ति  </h3></a>
			</div>
		</div>
		<div class="col-sm-3">
			<div class="row">
				<img src="image/headline7.jpg" width="100%">
			</div>
			<div class="row">
				<a href=""><h3 align="center">बजेटपछि प्रदेश २ मा होला सरकार हेरफेर ?   </h3></a>
			</div>
		</div>
				<div class="col-sm-3">
			<div class="row">
				<img src="image/headiline6.jpg" width="100%">
			</div>
			<div class="row">
				<a href=""><h3 align="center">संविधानले स्वीकारेको सङ्घीयता काँग्रेसले कहिले स्वीकार्छ ?   </h3></a>
			</div>
		</div>
				<div class="col-sm-3">
			<div class="row">
				<img src="image/headline5.jpg" width="100%">
			</div>
			<div class="row">
				<a href=""><h3 align="center">प्गुठी विधेयकको विरोधमा हजारौं पाटनवासी सडकमाः फिर्ता नलिए कडा आन्दोलन गर्ने चेतावनी (भिडियो)   </h3></a>
			</div>
		</div>
	</div>
	<!--HeadLine End  -->
<div class="row add3">
	<img src="image/ad3.gif" class="img-responsive center-block">
</div>
<!-- News Start -->
	
	<div class="row headline">
		<div class="col-sm-6">
			<button class="btn btn-danger btn-lg">समाचार</button>

		</div>
		<div class="col-sm-6" align="right">
			<button class="btn btn-primary btn-md moreButton">सबै </button>
		</div>
	</div>
	<hr>
	<div class="row samachgar">
		<div class="col-sm-6">
			<img src="image/sama1.jpg" width="100%">
		</div>
		<div class="col-sm-6">
			<a href=""><h3>माछापुच्छ्रे बैंकले थप्यो चार शाखा, १२ शाखारहित बैंकिङ् सेवा</h3></a>
			<p>काठमाडौं । माछापुच्छ्रे बैंक लिमिटेडले प्रदेश नं १ का सुनसरी र झापा जिल्लामा थप चार शाखा कार्यालयहरु शुभारम्भ गरेको छ । बैंकले सुनसरी इनरुवा नगरपालीकास्थित इनरुवा र सोही जिल्ला कै बराहाक्षेत्र नगरपालिकास्थित चक्रघट्टी तथा झापाको भद्रपुर नगरपालिका १०, स्थित चन्द्रगढी (चाराली रोड) र सोहि जिल्ला कै मेचीनगर नगरपालिकास्थित काँकरभिट्टा शाखा कार्यालयको उद्घाटन गरेको हो । <br>

			सुनसरी इनरुवास्थित शाखा कार्यालयको उद्घाटन  इनरुवा नगरपालिकाका मेयर राजन मेहता तथा चक्रघट्टी शाखा कार्यालयको  उद्घाटन बराहाक्षेत्र नगरपालिकाका मेयर निलम खनालले उदेघाटन गरे ।<br>

			त्यसैगरि झापाको चन्द्रगढी शाखा कार्यालयको उद्घाटन  भद्रपुर नगरपालिकाका मेयर जीवन श्रेष्ठ र झापा जिल्लाका प्रमुख जिल्ला अधिकारी जनकराज दाहालले संयुक्तरुपमा गरे , भने काँकरभिट्टा शाखा कार्यालयको मेचीनगर नगरपालीकाका मेयर बिमल आचार्यले गरे । </p>
		</div>
	</div>
	<div class="row spadd">
		<div class="col-sm-6">
			<img src="image/ad10.jpeg">
		</div>
		<div class="col-sm-6">
			<img src="image/ad11.jpg">
		</div>
	</div>
	<div class="row samachgarImage">
		<div class="col-sm-3">
			<div class="row">
				<img src="image/sama2.jpg" width="100%">
			</div>
			<div class="row">
				<a href=""><h3 align="center">लिच्चीको कारण भारतमा किन मरिरहेका छन् बालबालिका ?</h3></a>
			</div>
		</div>
		<div class="col-sm-3">
			<div class="row">
				<img src="image/sama5.jpg" width="100%">
			</div>
			<div class="row">
				<a href=""><h3 align="center">जसले शीतललाई शीतल‘जी’ बनायो</h3></a>
			</div>
		</div>
		<div class="col-sm-3">
			<div class="row">
				<img src="image/sama4.jpg" width="100%">
			</div>
			<div class="row">
				<a href=""><h3 align="center">जो नेपाली नोटका डिजाइनर थिए</h3></a>
			</div>
		</div>
		<div class="col-sm-3">
			<div class="row">
				<img src="image/sama3.jpg" width="100%">
			</div>
			<div class="row">
				<a href=""><h3 align="center">विप्लव समूहमाथिको प्रतिबन्ध पुनर्विचार गर्न सरकारलाई जर्मनको कम्युनिस्ट पार्टीको आग्रह </h3></a>
			</div>
		</div>
	</div>
<!-- News End -->
	
	<div class="row add3">
	<img src="image/ad3.gif" class="img-responsive center-block">
</div>
		<!--Suchana Prabadhi Start  -->

	<div class="row headline">
		<div class="col-sm-6">
			<button class="btn btn-danger btn-lg">सूचना प्रविधि</button>

		</div>
		<div class="col-sm-6" align="right">
			<button class="btn btn-primary btn-md moreButton">सबै </button>
		</div>
	</div>
	<hr>
	<div class="row suchanaPrabadi">
		<div class="col-sm-4">
			<div class="row">
				<img src="image/suchanaparbadi1.webp" width="100%">
			</div>
			<div class="row">
				<a href=""><h3 align="center">एनसेल फोरजीमा अब डबल डेटा !</h3></a>
			</div>
			<div class="row">
				<p align="center">एनसेल प्राईभेट लिमिटेडले आफ्ना ग्राहकका लागि डेटामा आकर्षक योजना ल्याएको छ जसअन्तर्गत ग्राहकले डबल फोरजी डेटाको मजा लिन सक्नेछन् । यो योजना अन्तर्गत एनसेलका ग्राहकले सात दिन र तीस दिन समयसिमा भएको गज्जबको डेटा प्याकको खरिदमा बोनस स्वरुप फोरजी डेटा भोलुम प्राप्त गर्न सक्ने एनसेलले जनाएको छ ।<br> <button class="btn btn-primary btn-md">More</button></p>
			</div>
		</div>
		<div class="col-sm-6">
			<div class="row">
				<div class="col-sm-6">
					<div class="row">
						<img src="image/suchanprabadi2.jpg" width="100%" height="220px">
					</div>
					<div class="row">
						<a href=""><h4 align="center">अब आइफोन र आइप्याड पनि माउसले चलाउन मिल्ने</h4></a>
					</div>
				</div>

				<div class="col-sm-6">
					<div class="row">
						<img src="image/suchanaparabdi4.webp" width="100%" height="220px">
					</div>
					<div class="row">
						<a href=""><h4 align="center">यी हुन् आर्टिफिसियल इन्टेलिजेन्स प्रविधिको सबैभन्दा धेरै पेटेन्ट लिने कम्पनीहरु</h4></a>
					</div>
				</div>
			</div>
			<div class="col-sm-6">
					<div class="row">
						<img src="image/suchanaprabadi3.webp" width="100%" height="220px">
					</div>
					<div class="row">
						<a href=""><h4 align="center">नासाले खोल्दैछ पर्यटकका लागि अन्तरीक्ष स्टेशन</h4></a>
					</div>
				</div>
				<div class="col-sm-6">
					<div class="row">
						<img src="image/suchanaprabadi6.webp" width="100%" height="220px">
					</div>
					<div class="row">
						<a href=""><h4 align="center">हुवावेलाई प्रयोगकर्ताको साथ, कम्पनीद्वारा ग्राहकलाई कुनै फरक नपर्ने ‘ग्यारेन्टी’</h4></a>
					</div>
				</div>
			</div>
				<div class="col-sm-2 treding" >
						<h2>समाचारहरु</h2>
						<table class="table table-striped">
								    <tbody>
								      <tr>
								        <td><a href="">विश्वकप क्रिकेटको अपडेट हाम्रो पात्रोमा</a></td>
								      </tr>
								      <tr>
								        <td><a href="">हुवावेको संकट झनै बढ्यो, स्मार्टफोनमा मेमोरी कार्ड हाल्न नपाउने</a></td>
								      </tr>
								      <tr>
								        <td><a href="">विश्वकप क्रिकेटको अपडेट हाम्रो पात्रोमा</a></td>
								      </tr>
								      <tr>
								        <td><a href="">‘टाइम्स अफ नेपाल’ एप सार्वजनिक</a></td>
								      </tr>
								      <tr>
								       <td><a href="">
										गुगलले बन्द गर्यो युट्युब गेमिङ एप</a></td>
								      </tr>
								      <tr>
								        <td><a href="">बजेट दियो टुटल र पठाओलाई राहत</a></td>
								      </tr>
								      <tr>
								        <td><a href="">फेसबुक संस्थापक जुकरबर्गलाई विश्वासको मत, अध्यक्षबाट हट्नु परेन</a></td>
								      </tr>
								      <tr>
								        <td><a href="">एनसेलको फोरजी अब देशको एक हजार बढी स्थानमा</a></td>
								      </tr>

								    </tbody>
								  </table>
				</div>
		</div>

	<!--Suchana Prabadhi End  -->

		<div class="row add3">
			<img src="image/ad4.gif" class="img-responsive center-block" >
		</div>
	<!-- Intreview Start -->

	<div class="row headline">
		<div class="col-sm-6">
			<button class="btn btn-danger btn-lg">अन्तर्वार्ता </button>

		</div>
		<div class="col-sm-6" align="right">
			<button class="btn btn-primary btn-md moreButton">सबै </button>
		</div>
	</div>
	<hr>
	<div class="row intreviews">
		<img src="image/intreview.jpg" class="img-responsive center-block" >
	</div>
	<!-- Intreview End -->
			<div class="row add3">
			<img src="image/ad5.gif" class="img-responsive center-block" >
		</div>
	<!-- Entertainment Start -->

	<div class="row headline">
		<div class="col-sm-6">
			<button class="btn btn-danger btn-lg">मनोरञ्जन </button>

		</div>
		<div class="col-sm-6" align="right">
			<button class="btn btn-primary btn-md moreButton">सबै </button>
		</div>
	</div>
	<hr>
	<div class="row entertainment">
		<div class="col-sm-6">
			<img src="image/namratashrestha.webp" class="img-responsive center-block" style="height: 600px;">
			<div class="text-block">
				<h3>सेतो पहिरनमा खुलेकी नम्रता </h3>
				
			</div>
		</div>
		<div class="col-sm-6">
			<div class="row">
				<div class="col-sm-6">
					<div class="row">
						<img src="image/entr.jpg" class="rounded-circle">
					</div>
					<div class="row">
						<a href=""><h3 align="center">‘प्रेमगीत ३’मा मुख्य अभिनेता मनिष</h3></a>
					</div>
				</div>
				<div class="col-sm-6 ">
					<div class="row ">
						<img src="image/entr2.jpg" class="rounded-circle">
					</div>
					<div class="row">
						<a href=""><h3 align="center">‘मधुपर्क सम्मान’बाट डा. विमल, सरिता र सुरेश सम्मानित </h3></a>
					</div>
				</div>

								<div class="col-sm-6">
					<div class="row">
						<img src="image/entr5.gif" class="rounded-circle">
					</div>
					<div class="row">
						<a href=""><h3 align="center">नेपाली गीतमा सनी लियोनीको नृत्य (भिडियोसहित) </h3></a>
					</div>
				</div>

								<div class="col-sm-6">
					<div class="row">
						<img src="image/entr4.webp" class="rounded-circle" >
					</div>
					<div class="row">
						<a href=""><h3 align="center">आमिरको चलचित्रमा करिना हिरोइन</h3></a>
					</div>
				</div>
			</div>
		</div>
	</div>
	<!-- Entertainment End -->
<div class="row add3">
	<img src="image/ad6.gif" class=" img-responsive center-block">
</div>
<!-- Sports Start -->
	
	<div class="row headline">
		<div class="col-sm-6">
			<button class="btn btn-danger btn-lg">खेल </button>

		</div>
		<div class="col-sm-6" align="right">
			<button class="btn btn-primary btn-md moreButton">सबै </button>
		</div>
	</div>
	<hr>
	<div class="row sport">
		<div class="col-sm-6">
			<img src="image/sp1.jpg" width="100%" >
		</div>
		<div class="col-sm-3">
			<a href=""><h3>वर्षाका कारण भारत र न्युजिल्याण्डको खेल रद्द</h3></a>
			<p>एजेन्सी । विश्वकप क्रिकेटमा बिहीबार हुने भनिएको भारत र न्युजिल्याण्डबीचको खेल रद्द भएको छ ।  निरन्तरको बर्षाले दुई टोलीबीचको खेल रद्द भएको हो ।<br> खेल रद्द भएपछि दुवै टिमले अंक बाँडेका छन् । बर्षाका कारण रद्द हुने यो चौथो खेल हो । इङ्ल्याण्डमा जारी आईसिसी एक दिवसीय विश्वकप क्रिकेटको चौथो खेल पनि वर्षाले प्रभावित भएको छ। बिहिबार हुने भनिएको भारत र न्यूजिल्याण्डबीचको खेल वर्षाका कारण रद्ध भएको छ। .......</p>
		</div>
		<div class="col-sm-3 smadd">
			<img src="image/smadd1.gif" width="100%"><br>
			<img src="image/smadd2.gif" width="100%">
		</div>
	</div>
	<div class="row sportlist">
		<div class="col-sm-3">
			<div class="row ">
				<img src="image/sp2.jpg" class="rounded-circle" width="100%">
			</div>
			<div class="row">
				<a href=""><h3 align="center">फिफा वरियतामा नेपाल ४ स्थान तल झर्‍यो, उत्कृष्ट चार देश यथावत</h3></a>
			</div>
		</div>
				<div class="col-sm-3">
			<div class="row ">
				<img src="image/sp4.jpg" class="rounded-circle" width="100%">
			</div>
			<div class="row">
				<a href=""><h3 align="center">शाहिदको चड्कन खाएपछि आमिरले कबुलेका थिए आफ्नो अपराध</h3></a>
			</div>
		</div>
				<div class="col-sm-3">
			<div class="row ">
				<img src="image/sp5.jpg" class="rounded-circle" width="100%">
			</div>
			<div class="row">
				<a href=""><h3 align="center">अंक बाँडेपनि यसकारण खुसी छन् दक्षिण अफ्रिकाका कप्तान</h3></a>
			</div>
		</div>
				<div class="col-sm-3">
			<div class="row ">
				<img src="image/sp3.jpg" class="rounded-circle" width="100%">
			</div>
			<div class="row">
				<a href=""><h3 align="center">सिन्धुपाल्चोकमा अन्तर्राष्ट्रिय ट्रेल रेस</h3></a>
			</div>
		</div>

	</div>
<!-- Sports End -->
<hr>
<!-- Special News start -->
	<div class="row special">
		<a href=""><h3 align="center"> कलाकार कृष्ण मल्लको जीवन : शत्रुघ्न सिन्हाले राजेश खन्नाकी प्रेमिका चिनाए</h3></a>
	</div>
	<div class="row special">
		<img src="image/special.jpg" width="100%">
	</div>
	<hr>
	<div class="row special">
		<a href=""><h3 align="center"> खानेलाई भन्दा नखानेलाई बढी पिर्ने धुम्रपान</h3></a>
	</div>
	<div class="row special">
		<img src="image/spceial2.jpg" width="100%">
	</div>
<!-- Special News End -->
<!-- Footer Start -->
<div class=" row fotter">
	<div class="col-sm-3">
		<h1 align="center">खबर आजको </h1>
		<p align="center">सबै भन्दा पहिले </p>
	</div>
	<div class="col-sm-3">
		<h3>सोसल मिडिया</h3><hr>
		<a href=""><h6> <i class="fa fa-facebook" style="font-size: 30px;"></i>  Facebook</h6></a>
		<a href=""><h6> <i class="fa fa-twitter" style="font-size: 30px;"></i>  Twitter</h6></a>
		<a href=""><h6> <i class="fa fa-linkedin" style="font-size: 30px;"></i>  linkedin</h6></a>
		<a href=""><h6> <i class="fa fa-instagram" style="font-size: 30px;"></i>  Instagram</h6></a>
	</div>
	<div class="col-sm-3">
		<div class="row">
			<div class="col-sm-6">
				<h3>प्रधान सम्पादक</h3><br><hr>
				<a href=""><h6>Amrit Kafle</h6></a>
			</div>
			<div class="col-sm-6">
				<h3>विज्ञापनका लागि</h3><hr>
				<h6>015544598, 9801123339, 9851123339 </h6>
			</div>
		</div>
	</div>
	<div class="col-sm-3">
		<h3>मेनु</h3>
		<div class="row">
			<div class="col-sm-6">
				<a href=""><h6>गृहपृष्ठ</h6></a>
				<a href=""><h6>ब्लग</h6></a>
				<a href=""><h6>साहित्यपाटी</h6></a>
				<a href=""><h6>पाठक विचार</h6></a>
				<a href=""><h6>फिड</h6></a>

			</div>
			<div class="col-sm-6">
				<a href=""><h6>विशेष</h6></a>
				<a href=""><h6>ICC #CT2017</h6></a>
				<a href=""><h6>सेभेन डेज इन टिबेट</h6></a>
				<a href=""><h6>Election 2017</h6></a>
				 <a href=""><h6>विश्वकप २०१८</h6></a>
			</div>
		</div>
	</div>
</div>
<div class="row copyright">
	<div class="col-sm-4">
		 
	</div>
	<div class="col-sm-4">
		<h3>© Amrit Kafle  Pvt. Ltd. -2019 All rights reserved.</h3>
	</div>
	<div class="col-sm-4">
		<a href=""><h3 align="right">Develop By Amrti kafle</h3></a>
	</div>
</div>
<!-- Footer End -->
</div>
</body>
</html>